package com.crawler.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.crawler.controller.MyWebCrawler;
import com.crawler.service.impl.CrawlerSerivceImpl;
import com.crawler.utils.ConfigParamter;
import com.crawler.utils.Result;
import com.crawler.utils.UrlTarget;

@ManagedBean
@ViewScoped
public class CrawlerMB implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String target;
	private CrawlerSerivceImpl crawlerSerivce;
	private ConfigParamter configParamter;
	private List<Result> top;
	private List<Result> allPages;
	private String  path;
	
	
	@PostConstruct
	public void init(){
	    path  = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/storg");
	    System.err.println("###########" + path  );
		crawlerSerivce = new CrawlerSerivceImpl();
		configParamter = new ConfigParamter();
		configParamter.setFolder(path);
		configParamter.setNumberOfCrawler(10);
	}
	public void search(){
		top =  new  ArrayList<Result>();
		allPages = new ArrayList<Result>();
	if (target != null) {
		UrlTarget.setUrl(target);
		configParamter.setDepth(1);
		configParamter.setPagesToFetch(200);
		crawlerSerivce.ConfigCrawling(configParamter);
		crawlerSerivce.startCrawling(MyWebCrawler.class, configParamter.getNumberOfCrawler());
		crawlerSerivce.finishCrawling();	
		top = crawlerSerivce.resultOfCrawrling();
		System.err.println("Top size : " + top.size());
		configParamter.setDepth(-1);
		configParamter.setPagesToFetch(-1);
		crawlerSerivce.ConfigCrawling(configParamter);
		crawlerSerivce.startCrawling(MyWebCrawler.class, configParamter.getNumberOfCrawler());
		crawlerSerivce.finishCrawling();	
		allPages = crawlerSerivce.resultOfCrawrling();
		System.err.println("All pages :" + allPages.size());	
	}	
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public CrawlerSerivceImpl getCrawlerSerivce() {
		return crawlerSerivce;
	}
	public void setCrawlerSerivce(CrawlerSerivceImpl crawlerSerivce) {
		this.crawlerSerivce = crawlerSerivce;
	}
	public ConfigParamter getConfigParamter() {
		return configParamter;
	}
	public void setConfigParamter(ConfigParamter configParamter) {
		this.configParamter = configParamter;
	}
	public List<Result> getTop() {
		return top;
	}
	public void setTop(List<Result> top) {
		this.top = top;
	}
	public List<Result> getAllPages() {
		return allPages;
	}
	public void setAllPages(List<Result> allPages) {
		this.allPages = allPages;
	}

	
}
