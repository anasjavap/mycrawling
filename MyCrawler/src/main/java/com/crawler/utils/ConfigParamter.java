package com.crawler.utils;

import java.io.Serializable;

public class ConfigParamter  implements Serializable{

	private String folder;
	private int depth;
	private int pagesToFetch;
	private int numberOfCrawler;
	
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public int getPagesToFetch() {
		return pagesToFetch;
	}
	public void setPagesToFetch(int pagesToFetch) {
		this.pagesToFetch = pagesToFetch;
	}
	public int getNumberOfCrawler() {
		return numberOfCrawler;
	}
	public void setNumberOfCrawler(int numberOfCrawler) {
		this.numberOfCrawler = numberOfCrawler;
	}
	
	

}
