package com.crawler.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.crawler.service.CrawlerSerivce;
import com.crawler.utils.ConfigParamter;
import com.crawler.utils.Result;
import com.crawler.utils.UrlTarget;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class CrawlerSerivceImpl implements CrawlerSerivce {

	private CrawlConfig config;
	private CrawlController controller;

	@Override
	public void ConfigCrawling(ConfigParamter paramter) {
		config = new CrawlConfig();
		config.setCrawlStorageFolder(paramter.getFolder());
		config.setMaxPagesToFetch(paramter.getPagesToFetch());
		config.setMaxDepthOfCrawling(paramter.getDepth());
	}

	@Override
	public void startCrawling(Class webCrawler, int numberOfCrawler){
		if (config != null) {
			PageFetcher pageFetcher = new PageFetcher(config);
			RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
			RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
			try {
				controller = new CrawlController(config, pageFetcher, robotstxtServer);
				controller.addSeed(UrlTarget.getUrl());
				controller.start(webCrawler, numberOfCrawler);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	@Override
	public void finishCrawling() {
		if (controller != null) {
			controller.shutdown();
		}
	}

	@Override
	public List<Result> resultOfCrawrling() {
		List<Result> results = new ArrayList<Result>();
		if (controller != null) {
			for (Object obj : controller.getCrawlersLocalData()) {
				if (obj instanceof Result) {
					results.add((Result)obj);	
				}
			}
		}
		return results;
	}
}
