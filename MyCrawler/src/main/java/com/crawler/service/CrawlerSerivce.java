package com.crawler.service;

import java.util.List;

import com.crawler.utils.ConfigParamter;
import com.crawler.utils.Result;

import edu.uci.ics.crawler4j.crawler.WebCrawler;

public interface CrawlerSerivce {
	
	List<Result> resultOfCrawrling();
	
	void ConfigCrawling(ConfigParamter paramter);
	
	void startCrawling(Class webCrawler,int numberOfCrawler)  throws Exception ;
	
	void finishCrawling();

}
