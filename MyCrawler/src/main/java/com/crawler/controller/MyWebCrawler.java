package com.crawler.controller;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import com.crawler.utils.Result;
import com.crawler.utils.UrlTarget;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.url.WebURL;

public class MyWebCrawler extends WebCrawler {
	
	private final static Pattern SUFFIX = Pattern.compile(".*(\\.(css|js|bmp|gif|jpe?g" + "|png|tiff?|mid|mp2|mp3|mp4"
            + "|wav|avi|mov|mpeg|ram|m4v|pdf" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");

	
	private Result result;
	
	@Override
	public boolean shouldVisit(Page page, WebURL url) {
		String link = url.getURL().toLowerCase();
		String website= UrlTarget.getUrl();
		Set<String> matchedUrl =new  HashSet<String>();
		if (link.startsWith(website)&& !SUFFIX.matcher(link).matches() && !link.equals(website)) {
			if (matchedUrl.add(link)) {
				return true;
			}
		}
		return false;
	}
	
	
	@Override
	public void visit(Page page) {
		 result = new Result();		
		 String url =page.getWebURL().getURL();
		 result.setUrl(url);
		 if (!getMyController().getCrawlersLocalData().contains(result)) {
			 getMyController().getCrawlersLocalData().add(result);
			 System.err.println(page.getWebURL().getURL());
		}
	}
		

}
